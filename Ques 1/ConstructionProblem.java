package com.gl.controller;

import java.util.Scanner;

public class ConstructionProblem {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the total no of floors in the building");
		int floors = scanner.nextInt();
		int[] floorSize = new int[floors + 1];
		for (int i = 1; i < floors + 1; i++) {
			System.out.println("Enter the floor size given on day : " + i);
			int f = scanner.nextInt();
			floorSize[f] = i;
		}
		System.out.println("The order of construction is as follows ");
		int j = floors;
		boolean flag;
		for (int i = 1; i <= floors; i++) {
			flag = false;
			System.out.println("Day " + i + " :");
			while (j >= 1 && floorSize[j] <= i) {
				flag = true;
				System.out.print(j + " ");
				j--;
			}
			if (flag == true) {
				System.out.println();
			}
		}
		scanner.close();

	}
}
